from django.urls import path

from adverts.views import (
    AdvertListView, AdvertCategoryListView, AdvertCityListView, AdvertUserListView, AdvertDetailView,
    AdvertCreateTemplateView, AdvertCreateView, CommentCreateView, ExtraCommentsAJAXView
)

app_name = 'adverts'

urlpatterns = [
    path('', AdvertListView.as_view(), name='adverts-list'),
    path('<slug:category_slug_name>', AdvertCategoryListView.as_view(), name='category-list'),
    path('city/<slug:city_slug_name>', AdvertCityListView.as_view(), name='city-list'),
    path('user/<str:fio>', AdvertUserListView.as_view(), name='user-list'),
    path('<slug:category_slug_name>/<slug:advert_slug_title>/<int:id>', AdvertDetailView.as_view(), name='detail'),
    path('advert/add', AdvertCreateTemplateView.as_view(), name='advert-add'),
    path('advert/create', AdvertCreateView.as_view(), name='advert-create'),
    path('comment/create', CommentCreateView.as_view(), name='comment-create'),
    path('ajax/extra-comments', ExtraCommentsAJAXView.as_view(), name='get-extra-comments'),
]
