from typing import Optional

from adverts.models import Advert
from doski.celery import app


@app.task(autoretry_for=(Exception,), max_retries=1)
def calculate_similarity_coefficient(adv_id: int) -> Optional[bool]:
    result = False
    advert = Advert.objects.filter(pk=adv_id).first()
    if advert:
        similar_adverts = Advert.get_trigram_similarity_adverts(advert.pk, advert.text)
        if similar_adverts:
            ids_and_ratios = []
            max_similarity_coefficient = max(list(map(lambda x: x.similarity, similar_adverts)))
            for _ in similar_adverts:
                ids_and_ratios.append(dict(advert_id=_.id, ratio=_.similarity))
            advert.similarity_coefficient = float(max_similarity_coefficient)
            # if max_similarity_coefficient < 0.75 and not is_user_anonymous(post.user.id):
            #     advert.is_moderate = True
            #     send_message_to_chat(advert.title)
            advert.similarity_posts = ids_and_ratios
            advert.save()
        else:
            advert.is_moderate = True
            advert.similarity_coefficient = 0.3
            advert.save()
        result = True
    return result
