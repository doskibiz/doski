from django import template

register = template.Library()


@register.filter
def add_str(arg1, arg2):
    """
    Объединяем arg1 & arg2 приводя к строке, потому что по умолчанию django это не умеет
    """
    return str(arg1) + str(arg2)
