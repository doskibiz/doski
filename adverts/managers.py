from random import randint

from django.db.models import QuerySet, Count, Max
from django.utils.timezone import now as tz_now

from adverts.choices import AdvertStatus, DisplayStatus


class CategoryManager(QuerySet):
    """
    Мэнеджер категорий
    """
    def only_visible(self):
        return self.filter(advert__is_moderate=True, status=DisplayStatus.VISIBLE).annotate(count_of_adverts=Count('advert'))

    def random_item(self):
        max_id = self.all().aggregate(max_id=Max('id'))['max_id']
        while True:
            pk = randint(1, max_id)
            category = self.filter(pk=pk).only('name', 'description', 'slug_name').first()
            if category:
                return category


class AdvertManager(QuerySet):
    """
    Мэнеджер объявлений
    """

    __expiration: str = "-expiration_date"

    def _get_adverts(self, _status: str):
        qs = self.only_moderated().filter(status=_status)
        if AdvertStatus.is_vip(_status) or AdvertStatus.is_pin(_status):
            qs = qs.filter(expiration_date__gte=tz_now()).order_by(self.__expiration)
        return qs

    def only_moderated(self):
        return self.select_related("category", "city", "user").filter(is_moderate=True)

    def get_adverts(self, status: str):
        if status:
            return self._get_adverts(status)


class CommentManager(QuerySet):
    """
    Мэнеджер комментариев
    """
    __creation: str = "-created_at"

    def only_moderated(self):
        return self.select_related('advert').filter(is_moderate=True)

    def get_reversed_order(self):
        return self.only_moderated().order_by(self.__creation)

    def get_extra_comments(self, start: int, stop: int):
        distinct = self.only_moderated().values('author').annotate(author_count=Count('author')).filter(author_count=1)
        return self.filter(author__in=[_['author'] for _ in distinct])[start:stop]
