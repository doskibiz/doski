class AdvertStatus:
    PIN = 'pin'
    VIP = 'vip'
    REGULAR = 'regular'

    ITEMS = (PIN, VIP, REGULAR)

    CHOICES = (
        (PIN, 'Закреп'),
        (VIP, 'Вип'),
        (REGULAR, 'Обычное')
    )

    @classmethod
    def default(cls):
        return cls.REGULAR

    @classmethod
    def is_vip(cls, status) -> bool:
        return status == cls.VIP

    @classmethod
    def is_pin(cls, status) -> bool:
        return status == cls.PIN

    @classmethod
    def is_regular(cls, status) -> bool:
        return status == cls.REGULAR


class DisplayStatus:
    """
    Статусы для категорий
    """
    HIDDEN = 'hidden'
    VISIBLE = 'visible'

    ITEMS = (HIDDEN, VISIBLE)

    CHOICES = (
        (VISIBLE, 'Видимая'),
        (HIDDEN, 'Скрытая')
    )

    @classmethod
    def default(cls):
        return cls.VISIBLE
