from django import forms
from django.contrib import admin, messages
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from mptt.admin import MPTTModelAdmin

from adverts.models import City, Category, Advert, Comment
from adverts.tasks import calculate_similarity_coefficient
from utils.admin import CreateUpdateDateTimeAdvertAdmin, UserGeoIPAdmin


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    prepopulated_fields = {'slug_name': ('name',)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    # list_display = ('name', 'new_adverts_count', 'category_adverts_count', 'status',)
    list_display = ('name', 'status',)
    prepopulated_fields = {'slug_name': ('name',)}
    search_fields = ('name',)

    # TODO: исрпавить qs_category_name пустой, из-за этого ошибка
    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #
    #     qs_not_moderated = qs.filter(advert__is_moderate=False).annotate(new_adverts_count=Count('advert'))
    #
    #     qs_category_name = qs.exclude(advert__is_moderate=False)\
    #         .annotate(new_adverts_count=Count('advert'))\
    #         .values_list('name', flat=True)\
    #         .order_by('name')
    #
    #     qs_moderated = qs.filter(name__in=qs_category_name).annotate(new_adverts_count=Value(0, output_field=IntegerField()))
    #     return qs_not_moderated.union(qs_moderated)
    #
    # def get_object(self, request, object_id, from_field=None):
    #     queryset = super(CategoryAdmin, self).get_queryset(request)
    #     model = queryset.model
    #     field = model._meta.pk if from_field else model._meta.get_field(from_field)
    #     try:
    #         object_id = field.to_python(object_id)
    #         return queryset.get(**{field.name: object_id})
    #     except (model.DoesNotExist, ValidationError, ValueError):
    #         return None

    @mark_safe
    def new_adverts_count(self, obj):
        if hasattr(obj, 'new_adverts_count'):
            new_adverts_count = obj.new_adverts_count
        else:
            new_adverts_count = 0
        return f'<a href="/admin/adverts/advert/?category__id__exact={obj.id}&is_moderate__exact=0">{new_adverts_count}</a>'

    def category_adverts_count(self, obj):
        return obj.advert_set.count()

    new_adverts_count.allow_tags = True
    new_adverts_count.short_description = 'Новых объявлений'
    category_adverts_count.short_description = 'Всего объявлений'
    new_adverts_count.admin_order_field = 'new_posts_count'


class AdvertCategoryAdminForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects, empty_label=None)

    class Meta:
        model = Category
        fields = ('name',)


@admin.register(Advert)
class AdvertAdmin(CreateUpdateDateTimeAdvertAdmin, UserGeoIPAdmin, admin.ModelAdmin):
    list_display = (
        'id', 'title', 'similarity_coefficient', 'category', 'is_moderate', 'hit_count', 'status', 'user', 'created_at',
    )
    list_display_links = ('id', 'title',)
    list_filter = ('category', 'is_moderate', 'status', 'created_at',)
    prepopulated_fields = {'slug_title': ('title',)}
    search_fields = ('title', 'user__email', 'email', 'id', 'fio')
    readonly_fields = ('similarity_coefficient',)
    actions = ['recalculate_similarity_coefficient']

    form = AdvertCategoryAdminForm

    fieldsets = (
        ('Работа с объявлением', {
            'fields': ('title', 'slug_title', ('category', 'city', 'status'), 'text',
                       ('similarity_coefficient', 'user', 'is_moderate'))
        }),
        ('Информация пользователя', {
            'fields': (('fio', 'email'), ('phone', 'user_url'), ('company_name', 'ogrn'), ('address', 'skype'))
         }),
        ('Статитсика объявления', {
            'fields': (('expiration_date', 'hit_count'), 'similarity_posts')
        })
    )

    def recalculate_similarity_coefficient(self, request, queryset):
        for item in queryset:
            calculate_similarity_coefficient.delay(item.id)
        self.message_user(request, _('Коэффициенты пересчитываются, это займет время'), messages.SUCCESS)
    recalculate_similarity_coefficient.short_description = _('Пересчитать коэффициент')


@admin.register(Comment)
class CommentAdmin(MPTTModelAdmin):
    list_display = ('author', 'email', 'advert_link', 'is_moderate', 'created_at', 'text')
    list_filter = ('is_moderate', 'created_at')
    search_fields = ('author', 'email', 'text')
    ordering = ('-created_at',)

    def advert_link(self, obj):
        if hasattr(obj.advert, 'id'):
            link = f'<a href="/admin/adverts/advert/{obj.advert.id}">{obj.advert.title}</a>'
        else:
            link = '<a href="#">Комментраий без объявления</a>'
        return link

    advert_link.allow_tags = True
    advert_link.short_description = 'Ссылка на пост'
