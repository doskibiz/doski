from django import forms
from django.conf import settings
from django.utils.translation import gettext as _

from adverts.models import Advert, Comment
from utils.forms import StyledModelForm


class AdvertCreateForm(StyledModelForm):
    """
    Форма добавления нового объявления
    """
    class Meta:
        model = Advert
        fields = ('title', 'category', 'city', 'text', 'fio', 'email', 'phone')
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Заголовок объявления'}),
            'category': forms.Select(),
            'city': forms.Select(),
            'text': forms.Textarea(attrs={'placeholder': 'Текст Вашего объявления'}),
            'fio': forms.TextInput(attrs={'placeholder': 'ФИО'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Адрес Вашей электронной почты'}),
            'phone': forms.TextInput(attrs={'placeholder': 'Номер телефона'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.update(self._get_additional_fields(settings.SITE_NAME.lower()))

    @staticmethod
    def _get_additional_fields(site_name: str) -> dict:
        default = ('finalcredit', 'megazaym', 'zaymuzaym')
        fields_for_site = {
            'magiamira': {
                'user_url': forms.CharField(
                    label=_('Адрес вашего сайта'),
                    widget=forms.TextInput(attrs={
                        'class': 'form-control',
                        'placeholder': 'например: https://example.com'
                    })
                ),
                'address': forms.CharField(
                    label=_('Контактный адрес'),
                    widget=forms.TextInput(attrs={
                        'class': 'form-control',
                        'placeholder': 'например: Москва, Большой басманный переулок 8'
                    })
                ),
                'skype': forms.CharField(
                    label=_('Ваш скайп логин'),
                    widget=forms.TextInput(attrs={
                        'class': 'form-control',
                        'placeholder': 'например: mySkypeName'
                    })
                )
            },
            'default': {
                'company_name': forms.CharField(
                    label=_('Название компании'),
                    widget=forms.TextInput(attrs={
                        'class': 'form-control',
                        'placeholder': 'например: ООО Ромашка'
                    }),
                    help_text=_('Название вашей компании без кавычек')
                ),
                'ogrn': forms.CharField(
                    label=_('ОГРН'),
                    widget=forms.TextInput(attrs={
                        'placeholder': 'например: 5187702086248',
                        'class': 'form-control',
                    }),
                    help_text=_('Пожалуйста заполните ОГРН в формате: С-ГГ-КК-НН-ХХХХХ-Ч')
                ),
            }
        }
        if site_name and site_name in default:
            fields = fields_for_site.get('default')
        else:
            fields = fields_for_site.get(site_name)
        return fields


class CommentCreateForm(forms.ModelForm):
    """
    Форма создания комментария для объявления
    """
    class Meta:
        model = Comment
        fields = ('author', 'email', 'text', 'advert', 'parent')
        widgets = {
            'author': forms.TextInput(attrs={
                'id': 'id_author',
                'class': 'form-control',
                'placeholder': 'ФИО'
            }),
            'email': forms.EmailInput(attrs={
                'id': 'id_email',
                'class': 'form-control',
                'placeholder': 'Адрес электронной почты'
            }),
            'text': forms.Textarea(attrs={
                'id': 'id_text',
                'class': 'form-control',
                'rows': '3',
                'placeholder': 'Текст комментария'
            }),
            'advert': forms.HiddenInput(),
            'parent': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
