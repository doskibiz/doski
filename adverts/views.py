from typing import Dict

from django.conf import settings as system_settings
from django.contrib import messages
from django.db.models import Count, F
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, TemplateView

from adverts.choices import AdvertStatus
from adverts.forms import AdvertCreateForm, CommentCreateForm
from adverts.models import Advert, Comment, Category, City
from adverts.tasks import calculate_similarity_coefficient
from settings.models import Settings, KeyItem
from utils.mixins import DynamicUrlPatternViewMixin, AjaxViewMixin, ColorSchemeMixin, TitleViewMixin
from utils.paginator import DiggPaginator
from utils.services import UserGeoIPService


class AdvertBaseListView(ColorSchemeMixin, DynamicUrlPatternViewMixin, TitleViewMixin, ListView):
    model = Advert
    context_object_name = 'adverts'
    paginate_by = Settings.get_value(KeyItem.ADVERTS_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = f'adverts/{system_settings.SITE_NAME}/list.html'
    app_name_for_url = 'adverts'
    __empty_string: str = ''

    def get_queryset(self):
        qs = super().get_queryset().only_moderated()
        return qs.annotate(number_of_comments=Count('comments'))

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        vip_adverts = self.get_queryset().get_adverts(AdvertStatus.VIP)[:Settings.get_value(KeyItem.MAX_VIP_ADVERTS)]
        pin_adverts = self.get_queryset().get_adverts(AdvertStatus.PIN)[:Settings.get_value(KeyItem.MAX_PIN_ADVERTS)]
        context.update(dict(vip_adverts=vip_adverts, pin_adverts=pin_adverts))
        return context


class AdvertListView(AdvertBaseListView):
    """
    Вывод всех объявлений
    """
    def set_page_meta(self) -> Dict[str, str]:
        return dict(
            title='Портал бесплатных объявлений',
            description=f'{str(self.request.get_host()).upper()} - это портал бесплатных объявлений',
            type='website'
        )


class AdvertCategoryListView(AdvertBaseListView):
    """
    Вывод всех объявлений по выбранной категории
    """
    def get_queryset(self):
        qs = super().get_queryset()
        category_slug_name = self.kwargs.get('category_slug_name')
        if category_slug_name:
            qs = qs.filter(category__slug_name=category_slug_name)
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        category_slug_name = self.kwargs.get('category_slug_name')
        return self._update_context(context, category_slug_name)

    def _update_context(self, context, category_slug_name):
        vip_adverts = context.get('vip_adverts')
        pin_adverts = context.get('pin_adverts')
        if vip_adverts or pin_adverts:
            vip_adverts = vip_adverts.filter(category__slug_name=category_slug_name)[:Settings.get_value(KeyItem.MAX_VIP_ADVERTS)]
            pin_adverts = pin_adverts.filter(category__slug_name=category_slug_name)[:Settings.get_value(KeyItem.MAX_PIN_ADVERTS)]
        category = Category.objects.filter(slug_name=category_slug_name).values('name', 'description').first()
        self.set_page_meta(**self._update_page_meta(
                category['name'] if category else self.__empty_string,
                category['description'] if category else self.__empty_string
        ))
        context.update(dict(vip_adverts=vip_adverts, pin_adverts=pin_adverts))
        return context

    def _update_page_meta(self, name: str, description: str) -> Dict[str, str]:
        return dict(
            title=f'{name} - все беспалтные объявления на {self.request.get_host()}',
            description=f'{description}',
            type='website'
        )


class AdvertCityListView(AdvertBaseListView):
    """
    Вывод всех объявлений по выбранному городу
    """
    def get_queryset(self):
        qs = super().get_queryset()
        city_slug_name = self.kwargs.get('city_slug_name')
        if city_slug_name:
            qs = qs.filter(city__slug_name=city_slug_name)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        city_slug_name = self.kwargs.get('city_slug_name')
        return self._update_context(context, city_slug_name)

    def _update_context(self, context, city_slug_name):
        vip_adverts = context.get('vip_adverts')
        pin_adverts = context.get('pin_adverts')
        if vip_adverts or pin_adverts:
            vip_adverts = vip_adverts.filter(city__slug_name=city_slug_name)[:Settings.get_value(KeyItem.MAX_VIP_ADVERTS)]
            pin_adverts = pin_adverts.filter(city__slug_name=city_slug_name)[:Settings.get_value(KeyItem.MAX_PIN_ADVERTS)]
        city_name = City.objects.filter(slug_name=city_slug_name).values_list('name', flat=True).first()
        self.set_page_meta(**self._update_page_meta(city_name))
        context.update(dict(
            vip_adverts=vip_adverts,
            pin_adverts=pin_adverts,
        ))
        return context

    def _update_page_meta(self, city_name: str) -> Dict[str, str]:
        return dict(
            title=f'{city_name} - все объявления на {self.request.get_host()}',
            description=f'{city_name} - все объявления в вашем городе на {self.request.get_host()}',
            type='website'
        )


class AdvertUserListView(AdvertBaseListView):
    """
    Вывод всех объявлений по выбранному пользователю
    """
    def get_queryset(self):
        qs = super().get_queryset()
        fio = self.kwargs.get('fio')
        if fio:
            qs = qs.filter(fio=fio)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        fio = self.kwargs.get('fio')
        return self._update_context(context, fio)

    def _update_context(self, context, fio: str):
        vip_adverts = context.get('vip_adverts')
        pin_adverts = context.get('pin_adverts')
        if vip_adverts or pin_adverts:
            vip_adverts = vip_adverts.filter(fio=fio)[:Settings.get_value(KeyItem.MAX_VIP_ADVERTS)]
            pin_adverts = pin_adverts.filter(fio=fio)[:Settings.get_value(KeyItem.MAX_PIN_ADVERTS)]
        self.set_page_meta(**self._update_page_meta(fio))
        context.update(dict(
            vip_adverts=vip_adverts,
            pin_adverts=pin_adverts,
        ))
        return context

    def _update_page_meta(self, fio: str) -> Dict[str, str]:
        return dict(
            title=f'Все объявления пользователя {fio} на {self.request.get_host()}',
            description=f'Все объявления от пользвоателя {fio} доступны всегда на {self.request.get_host()}',
            type='website'
        )


class AdvertDetailView(ColorSchemeMixin, DynamicUrlPatternViewMixin, TitleViewMixin, DetailView):
    """
    Детальный просмотр объявления
    """
    model = Advert
    pk_url_kwarg = 'id'
    query_pk_and_slug = True
    template_name = f'adverts/{system_settings.SITE_NAME}/advert.html'
    form_class = CommentCreateForm
    app_name_for_url = 'adverts'

    def get_object(self, queryset=None):
        _id = self.kwargs.get('id')
        advert = get_object_or_404(Advert, pk=_id)
        advert.hit_count = F('hit_count') + 1
        advert.save(update_fields=['hit_count'])
        advert.refresh_from_db()
        return advert

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context.update(dict(
            similar_adverts=self.object.get_similar_adverts(self.object.id, self.object.category.slug_name),
            advert_comments=self.object.comments.filter(advert__id=self.object.id, is_moderate=True),
            comment_form=CommentCreateForm(initial={'advert': self.object.pk})
        ))
        return context

    def set_page_meta(self, **kwargs_meta) -> Dict[str, str]:
        kwargs_meta.update(dict(
            title=f'{self.get_object().title} - в категории {self.get_object().category.name.lower()} на {self.request.get_host()}',
            description=f'{self.get_object().category.description} на {self.request.get_host()}',
            type='article'
        ))
        return super().set_page_meta(**kwargs_meta)


class AdvertCreateTemplateView(ColorSchemeMixin, TitleViewMixin, TemplateView):
    """
    Страница с формой подачи объявления
    """
    template_name = f'adverts/{system_settings.SITE_NAME}/create.html'
    meta = dict(
        title='Подать объявления в любую категорию бесплатно',
        description='Подача бесплатных объявлений в любую категорию',
        type='website'
    )

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context.update(dict(create_advert_form=AdvertCreateForm()))
        return context


class AdvertCreateView(CreateView):
    """
    Вью для создания объявления
    """
    model = Advert
    form_class = AdvertCreateForm
    http_method_names = ['post']
    template_name = 'blocks/forms/_create_advert.html'
    success_url = reverse_lazy('adverts:advert-add')
    success_message = 'Ваше объявление успешно добавлено и проходит процедуру модерации'

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        geo_data = UserGeoIPService().get_data(self.request)
        if geo_data:
            new_advert = form.save()
            UserGeoIPService().save_model(geo_data, new_advert)
        else:
            new_advert = form.save()
        calculate_similarity_coefficient.delay(adv_id=new_advert.id)
        return super().form_valid(form)


class CommentCreateView(CreateView):
    """
    Вью для созлани комментария к объявлению
    """
    model = Comment
    form_class = CommentCreateForm
    http_method_names = ['post']
    template_name = 'blocks/advert/comments/_form.html'
    success_message = 'Ваш комментарий успешно добавлен'

    def get_success_url(self):
        return reverse_lazy('adverts:detail', kwargs={
            'category_slug_name': self.object.advert.category.slug_name,
            'advert_slug_title': self.object.advert.slug_title,
            'id': self.object.advert.pk
        })

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        geo_data = UserGeoIPService().get_data(self.request)
        if geo_data:
            new_comment = form.save()
            UserGeoIPService().save_model(geo_data, new_comment)
        return super().form_valid(form)


class ExtraCommentsAJAXView(AjaxViewMixin, View):
    """
    Ajax получение дополнительных комментариев в сайдбар
    """
    model = Comment

    def post(self, *args, **kwargs):
        if self.request.is_ajax():
            extra_comments = int(self.request.POST.get('extra_comments_count', None))
            start = int(Settings.get_value(KeyItem.MAX_SIDEBAR_COMMENTS))
            stop = start + extra_comments
            comments = self.model.objects.get_extra_comments(start, stop)
            if comments.exists():
                response = render_to_string('blocks/sidebar/_extra_comments.html', {'extra_comments': comments})
            else:
                response = comments.exists()
            return JsonResponse({'comments': response})
