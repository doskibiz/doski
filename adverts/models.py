from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.search import TrigramSimilarity
from django.core.validators import EmailValidator, MaxLengthValidator
from django.db import models
from django.db.models import Count
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.utils.translation import gettext as _
from mptt.models import MPTTModel, TreeForeignKey
from unidecode import unidecode

from adverts.choices import AdvertStatus, DisplayStatus
from adverts.managers import AdvertManager, CommentManager, CategoryManager
from utils.models import CreateUpdateDateTimeAbstract, UserGeoIPAbstract, UserAdditionalInformation


class City(models.Model):
    """
    Модель города
    """
    name = models.CharField(verbose_name=_('Название'), max_length=128)
    slug_name = models.SlugField(verbose_name=_('Слуг'), max_length=256)
    status = models.CharField(verbose_name=_('Статус'), max_length=16,
                              choices=DisplayStatus.CHOICES, default=DisplayStatus.VISIBLE)

    class Meta:
        ordering = ('name',)
        verbose_name = _('Город')
        verbose_name_plural = _('Города')

    def __str__(self):
        return f'{self.name}'


class Category(models.Model):
    """
    Модель категорий постов
    """
    name = models.CharField(verbose_name=_('Название'), max_length=128)
    slug_name = models.SlugField(verbose_name=_('Слуг'), max_length=256)
    status = models.CharField(verbose_name=_('Статус'), max_length=16,
                              choices=DisplayStatus.CHOICES, default=DisplayStatus.VISIBLE)
    description = models.TextField(verbose_name=_('Описание'), max_length=1024, default='')

    objects = CategoryManager.as_manager()

    class Meta:
        verbose_name = _('Категория')
        verbose_name_plural = _('Категории')

    def __str__(self):
        return f'{self.name}'

    def is_hidden(self) -> bool:
        return self.status == DisplayStatus.HIDDEN

    def is_visible(self) -> bool:
        return self.status == DisplayStatus.VISIBLE


class Advert(CreateUpdateDateTimeAbstract, UserGeoIPAbstract, UserAdditionalInformation, models.Model):
    """
    Модель объявлений пользователя
    """
    title = models.CharField(
        verbose_name=_('Заголовок'),
        max_length=256,
        validators=[MaxLengthValidator],
        help_text=_('Заголовок вашего объявления')
    )
    slug_title = models.SlugField(verbose_name=_('Слуг'), max_length=512)
    text = models.TextField(
        verbose_name=_('Текст'),
        max_length=2048,
        help_text=_('Подробно составленное объявление работает значительно лучше')
    )
    category = models.ForeignKey(Category, verbose_name=_('Категория'), on_delete=models.CASCADE)
    city = models.ForeignKey(City, verbose_name=_('Город'), on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Пользователь'),
        on_delete=models.SET_NULL,
        related_name='advert',
        null=True,
        blank=True
    )
    status = models.CharField(
        verbose_name=_('Статус'),
        max_length=16,
        choices=AdvertStatus.CHOICES,
        default=AdvertStatus.REGULAR
    )
    similarity_coefficient = models.FloatField(
        verbose_name=_('Коэффициент схожести'),
        default=0.0,
        null=True,
        blank=True,
        editable=False
    )
    similarity_posts = JSONField(verbose_name=_('Похожие посты'), null=True, blank=True)
    expiration_date = models.DateTimeField(verbose_name=_('Дата окончания'), blank=True, null=True)
    is_moderate = models.BooleanField(verbose_name=_('Модерация'), default=False)
    hit_count = models.PositiveIntegerField(verbose_name=_('Количество просмотров'), blank=True, null=True, default=0)

    objects = AdvertManager.as_manager()

    class Meta:
        ordering = ('-created_at',)
        verbose_name = _('Объявление')
        verbose_name_plural = _('Объявления')

    def __str__(self) -> str:
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse_lazy('adverts:detail', kwargs={
            'category_slug_name': self.category.slug_name,
            'advert_slug_title': self.slug_title,
            'id':  self.pk
        })

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.slug_title = slugify(unidecode(self.title), allow_unicode=True)
        super().save(force_insert, force_update, using, update_fields)

    def is_vip(self) -> bool:
        return self.status == AdvertStatus.VIP

    def is_pin(self) -> bool:
        return self.status == AdvertStatus.PIN

    def is_regular(self) -> bool:
        return self.status == AdvertStatus.REGULAR

    @classmethod
    def get_similar_adverts(cls, pk: int, category: str):
        return cls.objects.get_adverts(AdvertStatus.REGULAR) \
                          .exclude(pk=pk) \
                          .filter(category__slug_name=category) \
                          .only('title', 'created_at', 'hit_count', 'text', 'category', 'city', 'user') \
                          .annotate(number_of_comments=Count('comments'))[:5]  # type: Advert

    def get_help_text(self, field_name: str) -> str:
        """
        Получаем имя поля, возвращаем его хелп текст
        """
        for field in self._meta.fields:
            if field.name == field_name:
                return field.help_text

    @classmethod
    def get_trigram_similarity_adverts(cls, pk: int, text: str):
        adverts = cls.objects.all() \
                             .exclude(pk=pk) \
                             .annotate(similarity=TrigramSimilarity('text', text)) \
                             .filter(similarity__gte=0.5)
        return adverts


class Comment(MPTTModel, UserGeoIPAbstract):
    """
    Модель комментариев
    """
    advert = models.ForeignKey(
        Advert,
        verbose_name=_('Объявление'),
        related_name='comments',
        on_delete=models.SET_NULL,
        null=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Пользователь'),
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True)
    author = models.CharField(verbose_name=_('Автор'), null=True, blank=True, max_length=64)
    email = models.EmailField(
        verbose_name=_('Email'),
        validators=[EmailValidator],
        help_text=_('Ваша электронная почта не публикуется')
    )
    text = models.TextField(verbose_name=_('Текст'))
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    is_moderate = models.BooleanField(verbose_name=_('Модерация'), default=False)

    objects = CommentManager.as_manager()
    
    class Meta:
        ordering = ('created_at',)
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')
    
    def __str__(self):
        return f'{self.email}'
