# Generated by Django 2.2.7 on 2019-11-20 15:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adverts', '0010_auto_20191119_2026'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='description',
            field=models.TextField(default='', max_length=1024, verbose_name='Описание'),
        ),
    ]
