from typing import Dict

from django.conf import settings as system_settings
from django.db.models import F
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView

from blogs.models import Blog, Tag
from settings.models import Settings, KeyItem
from utils.mixins import TitleViewMixin, ColorSchemeMixin
from utils.paginator import DiggPaginator


class BlogListView(ColorSchemeMixin, TitleViewMixin, ListView):
    model = Blog
    paginate_by = Settings.get_value(KeyItem.BLOG_POST_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = f'blogs/{system_settings.SITE_NAME}/list.html'

    def set_page_meta(self, **kwargs_meta) -> Dict[str, str]:
        kwargs_meta.update(dict(
            title=f'Все статьи на {self.request.get_host()}',
            description=f'Актуальные финансовые статьи на {self.request.get_host()}',
            type='website'
        ))
        return super().set_page_meta(**kwargs_meta)


class BlogByTagListView(ColorSchemeMixin, TitleViewMixin, ListView):
    model = Blog
    paginate_by = Settings.get_value(KeyItem.BLOG_POST_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = f'blogs/{system_settings.SITE_NAME}/list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        tag_slug_name = self.kwargs.get('tag_name')
        tag = get_object_or_404(Tag, slug_name=tag_slug_name)
        self.set_page_meta(**self._update_page_meta(tag.name))
        context.update({
            'object_list': self.object_list.filter(tags__slug_name=tag_slug_name),
        })
        return context

    def _update_page_meta(self, tag_name: str) -> Dict[str, str]:
        return dict(
            title=f'Все статьи по тегу {tag_name}',
            description=f'Актуальные финансовые статьи по тегу {tag_name} на {self.request.get_host()}',
            type='website'
        )


class BlogDetailView(ColorSchemeMixin, TitleViewMixin, DetailView):
    model = Blog
    pk_url_kwarg = 'id'
    query_pk_and_slug = True
    template_name = f'blogs/{system_settings.SITE_NAME}/post.html'
    __empty_string: str = ''

    def get_object(self, queryset=None):
        slug_title = self.kwargs.get('blog_title')
        post = get_object_or_404(Blog, slug_title=slug_title)
        post.hit_count = F('hit_count') + 1
        post.save(update_fields=['hit_count'])
        post.refresh_from_db()
        return post

    def set_page_meta(self, **kwargs_meta) -> Dict[str, str]:
        kwargs_meta.update(dict(
            title=self.get_object().title,
            description=self.get_object().short_description if self.get_object().short_description else self.__empty_string,
            keywords=self.get_object().tags.all().values_list('name', flat=True),
            type='article',
            img=self.get_object().poster
        ))
        return super().set_page_meta(**kwargs_meta)
