from django.urls import path

from blogs.views import BlogListView, BlogByTagListView, BlogDetailView

app_name = 'blogs'

urlpatterns = [
    path('', BlogListView.as_view(), name='list'),
    path('tag/<slug:tag_name>', BlogByTagListView.as_view(), name='by_tag'),
    path('<slug:blog_title>', BlogDetailView.as_view(), name='detail'),
    # path('<slug:blog_title>/<int:id>', BlogDetailView.as_view(), name='detail'),
]
