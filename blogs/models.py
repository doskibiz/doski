from django.db import models
from django.urls import reverse_lazy
from django.utils.translation import gettext as _
from sorl.thumbnail import ImageField

from utils.models import CreateUpdateDateTimeAbstract


class Tag(models.Model):
    name = models.CharField(verbose_name=_('Название'), max_length=64)
    slug_name = models.SlugField(verbose_name=_('Слуг'), max_length=128)

    class Meta:
        verbose_name = _('Таг')
        verbose_name_plural = _('Таги')

    def __str__(self):
        return f'{self.name}'


class Blog(CreateUpdateDateTimeAbstract, models.Model):
    title = models.CharField(verbose_name=_('Заголовок'), max_length=128)
    slug_title = models.SlugField(verbose_name=_('Слуг'), max_length=512)
    short_description = models.TextField(verbose_name=_('Краткое описание'), max_length=256, null=True, blank=True, default='')
    text = models.TextField(verbose_name=_('Текст'), max_length=8192)
    tags = models.ManyToManyField(Tag, verbose_name=_('Таги'))
    hit_count = models.PositiveIntegerField(verbose_name=_('Колличество просмотров'), default=0, blank=True, null=True)
    poster = ImageField(verbose_name=_('Постер для статьи'), upload_to='posters', blank=True, null=True)

    class Meta:
        ordering = ('-created_at',)
        verbose_name = _('Блог')
        verbose_name_plural = _('Блоги')

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse_lazy('blogs:detail', kwargs={
            'blog_title': self.slug_title,
        })
