from django.contrib import admin

from blogs.models import Blog, Tag
from utils.admin import CreateUpdateDateTimeAdvertAdmin


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug_name']
    search_fields = ('name',)
    prepopulated_fields = {'slug_name': ('name',)}


@admin.register(Blog)
class BlogAdmin(CreateUpdateDateTimeAdvertAdmin, admin.ModelAdmin):
    list_display = ['title', 'hit_count', 'created_at', 'updated_at', 'created_by', 'updated_by']
    search_fields = ('title', 'tags',)
    prepopulated_fields = {'slug_title': ('title',)}
    filter_horizontal = ['tags']

    fieldsets = (
        (None, {
            'fields': ('title', 'slug_title', 'text', 'tags', 'poster')
        }),
        (None, {
            'fields': ('hit_count',)
        }),
    )

