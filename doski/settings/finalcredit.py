import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

from .common import *

env.read_env(os.path.join(BASE_DIR, '.env_finalcredit'))
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('SECRET_KEY', default='')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', default=False)

# ALLOWED HOSTS
ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default='example.com')

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': env.db()
}

# SITE NAME PARAM
SITE_NAME = env.str('SITE_NAME', default='default')

# DOMAIN
DOMAIN = env.str('DOMAIN', default='localhost')

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles/' + SITE_NAME))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL += SITE_NAME + '/'
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS.append(str(ROOT_DIR('assets/' + SITE_NAME)))
STATICFILES_DIRS.append(str(ROOT_DIR('assets/shared')))


# DJANGO DEBUG TOOLBAR
# ------------------------------------------------------------------------------
if DEBUG:
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
    INSTALLED_APPS += ('debug_toolbar',)

    INTERNAL_IPS = ('127.0.0.1',)
else:
    # Sentry CONFIGURATION
    # ------------------------------------------------------------------------------
    sentry_sdk.init(
        dsn=env.str('SENTRY_DSN', default=''),
        integrations=[DjangoIntegration(), CeleryIntegration(), RedisIntegration()],

        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True
    )


# CACHE
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': env.CACHE_SCHEMES['rediscache'],
        'LOCATION': f'{env.str("REDIS_URL")}/1',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'PICKLE_VERSION': -1
        },
        'KEY_PREFIX': f'{SITE_NAME}'
    },
}


# SCHEME
# ------------------------------------------------------------------------------
USE_HTTPS = env.bool('USE_HTTPS', default=False)


# Celery
# ------------------------------------------------------------------------------
CELERY_BROKER_URL = f'{env.str("REDIS_URL")}/0'
CELERY_RESULT_BACKEND = f'{env.str("REDIS_URL")}/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE


# Email
# ------------------------------------------------------------------------------
EMAIL_HOST = env.str('EMAIL_HOST', default='smtp.example.com')
EMAIL_PORT = env.int('EMAIL_PORT', default=587)
EMAIL_HOST_USER = env.str('EMAIL_HOST_USER', default='info@example.com')
EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = env.str('DEFAULT_FROM_EMAIL', default='info@example.com')
EMAIL_USE_TLS = env.bool('EMAIL_USE_TLS', default=True)
EMAIL_BACKEND = env.str('EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
