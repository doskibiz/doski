from django.contrib import sitemaps

from adverts.models import Advert
from blogs.models import Blog


class BlogSitemap(sitemaps.Sitemap):
    priority = 1
    changefreq = 'weekly'

    def items(self):
        return Blog.objects.all()

    def lastmod(self, obj):
        return obj.created_at


class AdvertSitemap(sitemaps.Sitemap):
    priority = 1
    changefreq = 'daily'

    def items(self):
        return Advert.objects.filter(is_moderate=True)

    def lastmod(self, obj):
        return obj.created_at
