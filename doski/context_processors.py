from django.conf import settings
from django.core.cache import cache
from django.http import HttpRequest
from django.utils.timezone import now as tz_now

from adverts.models import Comment, Category
from blogs.models import Blog
from settings.models import Settings, KeyItem


def sidebar_comments(request: HttpRequest):
    c = cache.get(f'{sidebar_comments.__name__}')
    if not c:
        c = Comment.objects.get_reversed_order()[:Settings.get_value(KeyItem.MAX_SIDEBAR_COMMENTS)]  # type: Comment
        cache.set(f'{sidebar_comments.__name__}', c, 60*5)
    return {'comments': c}


def sidebar_categories(request: HttpRequest):
    c = cache.get(f'{sidebar_categories.__name__}')
    if not c:
        c = Category.objects.only_visible()  # type: Category
        cache.set(f'{sidebar_categories.__name__}', c, 60*15)
    return {'categories': c}


def most_viewed_blog_post(request: HttpRequest):
    b = Blog.objects.filter(created_at__month=tz_now().month - 1).order_by('hit_count')[:3]  # type: Blog
    if not b:
        b = Blog.objects.filter(created_at__month=tz_now().month).order_by('hit_count')[:3]
        if b.count() < 3:
            b = Blog.objects.all()[:3]
    return {'viewed_blog_posts': b}


def category_description(request: HttpRequest):
    # TODO: Переписать
    category_slug = request.path.split('/')[1]
    if category_slug:
        c = cache.get(f'{category_description.__name__}:{category_slug}')
        if not c:
            if Category.objects.filter(slug_name=category_slug).exists():
                c = Category.objects.filter(slug_name=category_slug).only('name', 'description').first()
                cache.set(f'{category_description.__name__}:{category_slug}', c, 60 * 60)
            else:
                # TODO: обработать empty sequence
                # c = choice(cache.keys(f'{category_description.__name__}:*'))
                if not c:
                    c = Category.objects.random_item()
                    cache.set(f'{category_description.__name__}:{c.slug_name}', c, 60 * 60)
    else:
        c = Category.objects.random_item()
        cache.set(f'{category_description.__name__}:{c.slug_name}', c, 60 * 60)
    return {'category': c}


def site_name(request: HttpRequest):
    return {'site_name': settings.SITE_NAME.lower()}

