class SiteName:
    __MEGAZAYM = 'megazaym'
    __FINALCREDIT = 'finalcredit'

    @classmethod
    def is_finalcredit(cls, name: str) -> bool:
        return name == cls.__FINALCREDIT

    @classmethod
    def is_megazaym(cls, name: str) -> bool:
        return name == cls.__MEGAZAYM


class ColorScheme:
    # TODO: переделать на фабрику или на явное создание объектов
    navbar = {
        'color': 'navbar-light',
        'background_color': 'bg-light',
        'btn': {'color': 'btn-dark'}
    }
    sidebar = {'badge': {'style': 'badge-pill', 'color': 'badge-primary'}}
    card_footer = {
        'btn': {'style': 'btn-outline-primary'},
        'badge': {'style': 'badge-pill', 'color': 'badge-primary'}
    }
    form = {'btn': {'color': 'btn-outline-dark'}}
    footer = {'background_color': 'bg-light', 'link_color': 'text-secondary'}

    def _default_css_classes(self):
        return dict(
            navbar=self.navbar,
            sidebar=self.sidebar,
            card_footer=self.card_footer,
            form=self.form,
            footer=self.footer
        )

    def setup_css_classes_for(self, site_name: str):
        if SiteName.is_megazaym(site_name):
            return Megazaym().get_color_settings()
        elif SiteName.is_finalcredit(site_name):
            return Finalcredit().get_color_settings()
        else:
            return self._default_css_classes()


class Finalcredit(ColorScheme):
    def _setup_css_classes(self):
        self.navbar.update({
            'color': 'navbar-light',
            'background_color': 'bg-light',
            'btn': {'color': 'btn-primary'}
        })
        self.sidebar.update({
            'badge': {'style': 'badge-pill', 'color': 'badge-primary'}
        })
        self.form.update({
            'btn': {'color': 'btn-primary'}
        })
        self.card_footer.update({
            'btn': {'style': 'btn-outline-light'}
        })

    def get_color_settings(self):
        self._setup_css_classes()
        return dict(
            navbar=self.navbar,
            sidebar=self.sidebar,
            card_footer=self.card_footer,
            form=self.form,
            footer=self.footer
        )


class Megazaym(ColorScheme):

    def _setup_css_classes(self):
        self.navbar.update({
            'color': 'navbar-dark',
            'background_color': 'bg-danger',
            'btn': {'color': 'btn-light'}
        })
        self.sidebar.update({
            'badge': {'style': 'badge-pill', 'color': 'badge-danger'}
        })
        self.card_footer.update({
            'btn': {'style': 'btn-outline-danger'},
            'badge': {'style': 'badge-pill', 'color': 'badge-danger'}
        })
        self.form.update({
            'btn': {'color': 'btn-danger'}
        })
        self.footer.update({
            'background_color': 'bg-danger',
            'link_color': 'text-light'
        })

    def get_color_settings(self):
        self._setup_css_classes()
        return dict(
            navbar=self.navbar,
            sidebar=self.sidebar,
            card_footer=self.card_footer,
            form=self.form,
            footer=self.footer
        )
