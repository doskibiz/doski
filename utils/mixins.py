from typing import Dict

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.views.generic.base import ContextMixin

from utils.color_schemes import ColorScheme


class DynamicUrlPatternViewMixin(ContextMixin):

    app_name_for_url: str = None

    def get_context_data(self, **kwargs):
        return super().get_context_data(url_pattern_for=self._get_url_pattern(), **kwargs)

    def _get_url_pattern(self) -> Dict[str, str]:
        return dict(
            category_list=f'{self.app_name_for_url}:category-list',
            city_list=f'{self.app_name_for_url}:city-list',
            user_list=f'{self.app_name_for_url}:user-list'
        )


class ColorSchemeMixin(ContextMixin):

    def get_context_data(self, **kwargs):
        return super().get_context_data(color_scheme=self._get_color_scheme(settings.SITE_NAME))

    def _get_color_scheme(self, site_name: str) -> Dict[str, str]:
        return ColorScheme().setup_css_classes_for(site_name.lower())


class TitleViewMixin(ContextMixin):
    """
    Миксин для добавления заголовка страницы в контекст
    """
    meta: Dict[str, str] = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(page_meta=self._get_page_meta()))
        return context

    def _get_page_meta(self) -> Dict[str, str]:
        return self.set_page_meta()

    def set_page_meta(self, **kwargs_meta) -> Dict[str, str]:
        self.meta.update(**kwargs_meta)
        return self.meta


class AjaxViewMixin:
    """
    Миксин для проверки ajax запросов
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            raise PermissionDenied('Метод доступн только для AJAX запросов')
        return super().dispatch(request, *args, **kwargs)
