class CreateUpdateDateTimeAdvertAdmin:
    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj=obj)
        t = type(fieldsets)
        return fieldsets + t(((None, {'fields': (('created_at', 'created_by'), ('updated_at', 'updated_by'))}),))

    def get_readonly_fields(self, request, obj=None):
        fields = super().get_readonly_fields(request, obj=obj)
        t = type(fields)
        return fields + t(('created_at', 'created_by', 'updated_at', 'updated_by'))

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.updated_by = request.user
        super().save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            if not instance.pk:
                instance.created_by = request.user
            instance.updated_by = request.user
            instance.save()
        formset.save_m2m()


class UserGeoIPAdmin:
    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj=obj)
        t = type(fieldsets)
        return fieldsets + t(((None, {'fields': ('geo_ip', ('geo_ip_country', 'geo_ip_city'))}),))

    def get_readonly_fields(self, request, obj=None):
        fieldsets = super().get_readonly_fields(request, obj=obj)
        t = type(fieldsets)
        return fieldsets + t(('geo_ip', 'geo_ip_country', 'geo_ip_city'))