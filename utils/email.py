from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import get_template
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from users.models import User


class EmailManager:
    """
    Менеджер отправки писем электронной почты
    """

    def send_activation_email(self, user: User, use_https: bool = settings.USE_HTTPS) -> None:
        """
        Отправка сообщения с данными для активации аккаунта
        :param user: пользователь кому отправить сообщение
        :param use_https: протокол HTTPS
        """
        template = get_template('users/activate_email.html')
        c = {
            'protocol': 'https' if use_https else 'http',
            'domain': settings.DOMAIN,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': default_token_generator.make_token(user),
            'user': user
        }
        message = template.render(c)
        self.email_user(user=user, subject='Подтверждение аккаунта', message=message, content_subtype='html')

    @staticmethod
    def email_user(user: User, subject: str, message: HttpResponse, from_email: str = None, **kwargs) -> None:
        """
        Метод отправки email
        :param user: пользователь
        :param subject: тема сообщения
        :param message: шаблон сообщения
        :param from_email: почта отправителя
        :param kwargs: дополнительные аргументы
        """
        msg = EmailMessage(
            subject=subject,
            body=message,
            from_email=from_email if from_email else settings.DEFAULT_FROM_EMAIL,
            to=[user.email]
        )
        if kwargs['content_subtype']:
            message.content_subtype = kwargs['content_subtype']
        msg.send()
