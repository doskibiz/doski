from django.conf import settings
from django.core.validators import EmailValidator, URLValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class CreateUpdateDateTimeAbstract(models.Model):
    created_at = models.DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('Дата обновления'), auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        editable=False,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Автор первой версии',
        null=True,
        blank=True,
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        editable=False,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name='Автор последнего изменения',
        null=True,
        blank=True,
    )

    class Meta:
        abstract = True


class UserGeoIPAbstract(models.Model):
    geo_ip = models.GenericIPAddressField(verbose_name=_('IP'), blank=True, null=True)
    geo_ip_country = models.CharField(verbose_name=_('Страна'), blank=True, null=True, max_length=128)
    geo_ip_city = models.CharField(verbose_name=_('Город'), blank=True, null=True, max_length=128)

    class Meta:
        abstract = True


class UserAdditionalInformation(models.Model):
    fio = models.CharField(verbose_name=_('ФИО'), max_length=256, default='')
    email = models.EmailField(verbose_name=_('Email'), validators=[EmailValidator], blank=True, null=True, default='')
    phone = models.CharField(verbose_name=_('Телефон'), max_length=32, default='')
    user_url = models.URLField(verbose_name=_('URL'), validators=[URLValidator], blank=True, null=True)
    address = models.CharField(verbose_name=_('Адрес приема'), max_length=128, blank=True, null=True, default='')
    skype = models.CharField(verbose_name=_('Skype'), max_length=64, blank=True, null=True, default='')
    company_name = models.CharField(
        verbose_name=_('Название компании'),
        max_length=1024,
        default='',
        blank=True,
        null=True,
        help_text='Название вашей компании'
    )
    ogrn = models.CharField(
        verbose_name=_('ОГРН'),
        max_length=20,
        default='',
        blank=True,
        null=True,
        help_text='Пожалуйста заполните ОГРН в формате: С-ГГ-КК-НН-ХХХХХ-Ч'
    )

    class Meta:
        abstract = True
