from typing import Type, Dict

from django.http import HttpRequest
from ipware import get_client_ip
from django.contrib.gis.geoip2 import GeoIP2


class UserGeoIPService:
    """
    Сервис получения гео данных пользователя по его IP
    """
    def __init__(self):
        self._geo_ip = GeoIP2()

    def get_data(self, request: Type[HttpRequest]) -> Dict[str, str]:
        geo_data = {}
        user_ip, is_routable = get_client_ip(request)
        if user_ip is not None and is_routable:
            geo_data.update({
                'country': self._geo_ip.country(user_ip).get('country_name'),
                'city': self._geo_ip.city(user_ip).get('city_name'),
                'ip': user_ip
            })
        return geo_data

    @staticmethod
    def save_model(data: dict, model):
        if data and model:
            model.geo_ip = data.get('ip')
            model.geo_ip_country = data.get('country')
            model.geo_ip_city = data.get('city')
            model.save(update_fields=['geo_ip', 'geo_ip_country', 'geo_ip_city'])
