const boards = {
    getExtraComments: () => {
        const heightEmptySpace = $('.container-fluid').height() - $('.last-comments').height();
        const commentItemHeightArray = [];
        $('.last-comments .list-group-item').each(function() {
            commentItemHeightArray.push($(this).height());
        });
        commentItemHeightArray.splice(commentItemHeightArray.indexOf(Math.min.apply(Math, commentItemHeightArray)), 1);
        const commentItemHeightAvg = commentItemHeightArray.reduce((acc, c) => acc + c, 0) / commentItemHeightArray.length;
        const extraCommentsCount = Math.round(heightEmptySpace / commentItemHeightAvg);
        const csrf = $('input[name="csrfmiddlewaretoken"]').val();
        $.ajax({
            url: '/ajax/extra-comments',
            type: 'POST',
            data: {extra_comments_count: extraCommentsCount, csrfmiddlewaretoken: csrf},
            success: response => {
                if (response.comments) {
                    $('.last-comments').append(response.comments);
                }
            },
            error: jqXHR => {
                console.error(jqXHR.responseText);
            }
        })
    },
    emulateRecordSigh: (className) => {
        setInterval(() => { $(className).toggleClass('on-air-rec'); }, 2233);
    },
    calculateTextProgress: (elementId) => {
        $(elementId).on('keyup', function () {
            const l = $(this).val().length;
            const p = boards.calculateTextDiff(l);
            $('.char-counter-bar').css({'width': + p + '%', 'background-color': boards.calculateColor(p)});
            $('.char-counter').css({'color': boards.calculateColor(p)}).empty().append(l);
        });
    },
    calculateTextDiff: (textLength) => {
        const TEXT_LENGTH = 400;
        const calc = (textLength * 100) / TEXT_LENGTH;
        return calc >= 100 ? 100 : Math.round(calc);
    },
    calculateColor: (percent) => {
        if (percent <= 30) return '#dc3545';
        if (percent <= 60) return '#ffc107';
        else return '#28a745';
    },
    checkAdBlock: () => {
        setTimeout(() => {
            if(document.getElementsByTagName("iframe").item(0) === null) boards.showDisableAdBlockMessage();
        }, 500);
    },
    showDisableAdBlockMessage: () => {
        const message = `<strong>Кажется ты используешь AdBlock!<br></strong>` +
            `${window.location.host} существует и развивается за счет доходов от рекламы. Добавь нас ` +
            'в исключения, помоги порталу быть еще лучше.';
        const spanClose = $('<span>').attr('aria-hidden', 'true').html('&times;');
        const buttonClose = $('<button>')
                .attr('type', 'button')
                .attr('class', 'close')
                .attr('data-dismiss', 'alert')
                .attr('aria-label', 'Close')
                .append(spanClose);
        const alertDiv = $('<div>')
                .attr('class', 'alert alert-info alert-dismissible fade show')
                .attr('role', 'alert')
                .append(buttonClose)
                .append(message);
        $('.adbmsg').prepend(alertDiv);
    }
};
