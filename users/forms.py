from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

from users.models import User
from utils.forms import StyledForm


class UserAuthenticationForm(StyledForm, AuthenticationForm):
    """
    Форма для входа пользователя в систему (Расширение стандартной формы 'AuthenticationForm')
    """
    remember_me = forms.BooleanField(label=_('Запомнить меня'), required=False, widget=forms.CheckboxInput())

    def __init__(self, request=None, *args, **kwargs):
        super().__init__(request, *args, **kwargs)
        self.fields['username'].widget.attrs['class'] += ' form-control floating-label-input'
        self.fields['username'].widget.attrs['placeholder'] = 'Email'
        self.fields['username'].widget.attrs['id'] = 'id_username'
        self.fields['username'].widget.attrs['type'] = 'email'

        self.fields['password'].widget.attrs['class'] += ' form-control floating-label-input'
        self.fields['password'].widget.attrs['placeholder'] = _('Пароль')
        self.fields['password'].widget.attrs['id'] = 'id_password'

        self.fields['remember_me'].widget.attrs['class'] += ' remember-me-checkbox'


class UserRegistrationForm(StyledForm, forms.ModelForm):
    """
    Форма для регистрации пользователя
    """
    password_repeat = forms.CharField(
        label=_('Повторите пароль'),
        widget=forms.PasswordInput(attrs={
            'id': 'id_password_repeat',
            'class': 'form-control floating-label-input',
        })
    )
    error_messages = {
        'duplicate_email': _('Такой email уже зарегестрирован в системе'),
        'passwords_mismatch': _('Пароли не совпадают')
    }

    class Meta:
        model = User
        fields = ('email', 'password', 'password_repeat')
        widgets = {
            'email': forms.EmailInput(attrs={
                'id': 'id_email',
                'class': 'form-control floating-label-input',
                'placeholder': _('Email')
            }),
            'password': forms.PasswordInput(attrs={
                'id': 'id_password',
                'class': 'form-control floating-label-input',
                'placeholder': _('Пароль')
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password_repeat'].widget.attrs['class'] += ' form-control floating-label-input'
        self.fields['password_repeat'].widget.attrs['placeholder'] = _('Повторите пароль')
        self.fields['password_repeat'].widget.attrs['id'] = 'id_password_repeat'

    def clean(self):
        """
        Проверка повторно введенного пароля
        """
        super().clean()

        password = self.cleaned_data.get('password')
        password_repeat = self.cleaned_data.get('password_repeat')
        if password and password_repeat:
            if password != password_repeat:
                self.add_error(None, self.error_messages.get('passwords_mismatch'))

        # Валидация пароля
        password = self.cleaned_data.get('password')
        password_validation.validate_password(password)

        return self.cleaned_data

    # def clean_email(self):
    #     email = self.cleaned_data.get('email')
    #     if User.objects.filter(email=email).exists():
    #         self.add_error(None, self.error_messages.get('duplicate_email'))
    #     return self.cleaned_data
