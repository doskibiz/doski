from urllib.parse import urlencode
from hashlib import md5
from django import template
from django.conf import settings
from django.utils.html import escape
from django.utils.safestring import mark_safe

register = template.Library()

GRAVATAR_URL_PREFIX = 'https://www.gravatar.com/'
GRAVATAR_DEFAULT_IMAGE = f'http://localhost:8002/assets/{settings.SITE_NAME}/img/user-circle-solid.svg' if not settings.DEBUG else ''
GRAVATAR_DEFAULT_RATING = 'g'
GRAVATAR_DEFAULT_SIZE = 80
GRAVATAR_IMG_CLASS = 'gravatar rounded mr-1'


def _get_gravatar_id(email: str):
    return md5(email.encode()).hexdigest()


def _imgclass_attr():
    return f' class="{GRAVATAR_IMG_CLASS}"' if GRAVATAR_IMG_CLASS else ''


def _wrap_img_tag(url: str, info: str, size: int):
    return f'<img src="{url}"{_imgclass_attr()} alt="Avatar for {info}" height="{size}" width="{size}"/>'


@register.simple_tag
def gravatar_for_email(email: str, size: int, rating: str = None):
    """
    Генерируем Gravatar URL по email адресу.
    Syntax:
        {% gravatar_for_email <email> [size] [rating] %}
    Example:
        {% gravatar_for_email someone@example.com 48 pg %}
    """
    gravatar_url = f'{GRAVATAR_URL_PREFIX}avatar/{_get_gravatar_id(email)}'

    parameters = [p for p in (
        ('d', GRAVATAR_DEFAULT_IMAGE),
        ('s', size or GRAVATAR_DEFAULT_SIZE),
        ('r', rating or GRAVATAR_DEFAULT_RATING),
    ) if p[1]]

    if parameters:
        gravatar_url += '?' + urlencode(parameters, doseq=True)

    return mark_safe(_wrap_img_tag(escape(gravatar_url), email.lower(), size))
