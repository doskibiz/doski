from django.urls import path

from users.views import (
    UserLoginView, UserLogoutView, UserProfileAdvertListView, UserProfileCityListView, UserProfileCategoryListView,
    UserProfileFioListView, UserRegistrationFormView, AccountActivationDoneTemplateView, AccountActivationConfirmView
)

app_name = 'users'

urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('logout/', UserLogoutView.as_view(), name='logout'),
    path('registration/', UserRegistrationFormView.as_view(), name='registration'),
    path('<int:user_id>/', UserProfileAdvertListView.as_view(), name='profile'),
    path('activate/done/', AccountActivationDoneTemplateView.as_view(), name='activate-done'),
    path('activate/confirm/<str:uidb64>/<str:token>/', AccountActivationConfirmView.as_view(), name='activation-confirm'),
    path('profile/city/<slug:city_slug_name>/', UserProfileCityListView, name='city-list'),
    path('profile/category/<slug:category_slug_name>/', UserProfileCategoryListView.as_view(), name='category-list'),
    path('profile/user/<str:fio>/', UserProfileFioListView.as_view(), name='user-list'),
]
