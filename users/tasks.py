from typing import Optional

from doski.celery import app
from users.models import User
from utils.email import EmailManager


@app.task(autoretry_for=(Exception,), max_retries=3)
def send_activation_email(email: str) -> Optional[bool]:
    """
    Задача на отправаку письма для активации
    :param email: email пользоватлея
    """
    if email:
        user = User.objects.filter(email=email).first()
        if user and not user.is_active:
            EmailManager().send_activation_email(user)
            return True
