from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import LoginView, LogoutView
from django.db.models import Count
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import ListView, FormView, TemplateView

from adverts.models import Advert, Comment
from settings.models import Settings, KeyItem
from users.forms import UserAuthenticationForm, UserRegistrationForm
from users.models import User
from users.tasks import send_activation_email
from utils.mixins import DynamicUrlPatternViewMixin
from utils.paginator import DiggPaginator


class UserLoginView(LoginView):
    """
    Представление для входа в систему
    """
    template_name = 'users/login.html'
    form_class = UserAuthenticationForm
    redirect_authenticated_user = True

    def form_valid(self, form):
        if 'remember_me' not in form.cleaned_data:
            self.request.session.set_expiry(settings.SET_EXPIRY)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('users:profile', kwargs={'user_id': self.request.user.pk})


class UserLogoutView(LogoutView):
    """
    Представления для выхода пользователя и редирект на главную страницу
    """
    next_page = reverse_lazy('adverts:adverts-list')


class UserRegistrationFormView(FormView):
    model = User
    form_class = UserRegistrationForm
    template_name = 'users/registration.html'
    success_url = reverse_lazy('users:activate-done')

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.set_password(form.cleaned_data.get('password'))
        user.save()
        send_activation_email.delay(email=user.email)
        return super().form_valid(form)


class SendActivationEmail(FormView):
    template_name = 'users/registration.html'
    success_url = reverse_lazy('users:activate-done')

    def form_valid(self, form):
        email = form.cleaned_data.get('email')
        send_activation_email.delay(email=email)
        return super().form_valid(form)


class AccountActivationDoneTemplateView(TemplateView):
    template_name = 'users/activate_done.html'
    message_subject = 'Мы отправили вам сообщение для подтверждения вашего аккаунта. Вы должны получить его в ближайшее время.'
    message_content = 'Если вы не получили письмо, пожалуйста, убедитесь, что вы ввели правильный адрес или проверьте папку "Спам".'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context.update(dict(
            message_subject=self.message_subject,
            message_content=self.message_content
        ))
        return context


class AccountActivationConfirmView(View):
    """
    Активация аккаунта пользователя
    """
    template_name = 'users/activate_done.html'

    def get(self, request, uidb64=None, token=None):
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.filter(pk=uid).first()
        if user and default_token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            context = dict(
                message_subject=_('Ваш аккаунт успешно подтвержден!'),
                message_content=_('Чтобы войти в систему, перейдите на страницу входа.'),
            )
            response = render(request, self.template_name, context)
        else:
            context = dict(
                message_subject=_('Пользователь не найден или не активирован вовремя.'),
                message_content=_('Пожалуйста, воспользуйтесь формой повторной активации аккаунта или попробуйте зарегистрироваться заново.'),
            )
            response = render(request, self.template_name, context)
        return response


class UserProfileAdvertListView(DynamicUrlPatternViewMixin, ListView):
    model = Advert
    context_object_name = 'adverts'
    paginate_by = Settings.get_value(KeyItem.ADVERTS_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = 'users/profile.html'
    app_name_for_url = 'users'

    def get_queryset(self):
        qs = super().get_queryset().filter(user__id=self.request.user.pk).annotate(number_of_comments=Count('comments'))
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        advert_ids = self.get_queryset().values_list('id', flat=True)
        context.update(dict(
            adverts_comments=Comment.objects.only_moderated().filter(advert_id__in=[_ for _ in advert_ids]),
        ))
        return context


class UserProfileCityListView(DynamicUrlPatternViewMixin, ListView):
    model = Advert
    context_object_name = 'adverts'
    paginate_by = Settings.get_value(KeyItem.ADVERTS_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = 'users/profile.html'
    app_name_for_url = 'users'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        city_slug_name = self.kwargs.get('city_slug_name')
        context.update(dict(
             adverts=Advert.objects.filter(user__id=self.request.user.pk, city__slug_name=city_slug_name)
                                   .annotate(number_of_comments=Count('comments')),
        ))
        return context


class UserProfileCategoryListView(DynamicUrlPatternViewMixin, ListView):
    model = User
    context_object_name = 'adverts'
    paginate_by = Settings.get_value(KeyItem.ADVERTS_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = 'users/profile.html'
    app_name_for_url = 'users'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        category_slug_name = self.kwargs.get('category_slug_name')
        context.update(dict(
            adverts=Advert.objects.filter(user__id=self.request.user.pk, category__slug_name=category_slug_name)
                                  .annotate(number_of_comments=Count('comments'))
        ))
        return context


class UserProfileFioListView(DynamicUrlPatternViewMixin, ListView):
    model = User
    context_object_name = 'adverts'
    paginate_by = Settings.get_value(KeyItem.ADVERTS_PER_PAGE)
    paginator_class = DiggPaginator
    template_name = 'users/profile.html'
    app_name_for_url = 'users'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        fio = self.kwargs.get('fio')
        context.update(dict(
            adverts=Advert.objects.filter(user__id=self.request.user.pk, fio=fio)
                                  .annotate(number_of_comments=Count('comments'))
        ))
        return context
